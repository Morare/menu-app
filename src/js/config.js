export const API_URL = "https://forkify-api.herokuapp.com/api/v2/recipes/"
export const TIMEOUT_SEC = 10; 
export const RES_PER_PAGE = 10;
export const KEY = "459f87ad-b8c2-41d3-aa8e-b61b49c24a88"
export const MODAL_CLOSE_SEC = 2500