import icons from "url:../img/icons.svg";
import "core-js/stable"
import "regenerator-runtime/runtime"
import * as model from "./model.js"
import recipeView from "./views/recipeView.js";
import searchView from "./views/searchView.js";
import resultsView from "./views/resultsView.js";
import bookmarksView from "./views/bookmarksView.js";
import paginationView from "./views/paginationView.js";
import addRecipeView from "./views/addRecipeView.js";

import { MODAL_CLOSE_SEC } from "./config.js";


const controlRecipes = async function() {
  try {
    const id = window.location.hash.slice(1);
   
    recipeView.renderSpinner()
    if(!id) return;   

    resultsView.update(model.getResultsPage())
  


    /// 1. Loading
   await model.loadRecipe(id)

      ///2 Rendering recipe
   recipeView.render(model.state.recipe);
     bookmarksView.update(model.state.bookmarks)
  }
  catch(err) {
    recipeView.renderError()
    console.log(err)
  }

};

const controlSearchResults = async function() {
  try {
    resultsView.renderSpinner()
    const query = searchView.getQuery();
    if(!query) return;
    await model.loadSearchResults(query);
    resultsView.render(model.getResultsPage(model.state.search.page))
    paginationView.render(model.state.search)
  }
  catch(err) {
    console.log(err)
  }
}

const controlServings = function(newServings) {
  console.log("S-a declansat")
  model.updateServings(newServings);
  // recipeView.render(model.state.recipe);
  recipeView.update(model.state.recipe);
}

const controlPagination = function(gotToPage) {
  resultsView.render(model.getResultsPage(gotToPage))
  paginationView.render(model.state.search)
}

const controlAddBookmark = function() {

  if(!model.state.recipe.bookmarked) {
    model.addBookmark(model.state.recipe);
  }
  else {
    model.deleteBookmark(model.state.recipe.id);
  }

  recipeView.update(model.state.recipe)

  bookmarksView.render(model.state.bookmarks)
}
const controlAddRecipe = function(newRecipe) {

  addRecipeView.renderSpinner()

  model.uploadRecipe(newRecipe)
       .then(() => {  
        recipeView.render(model.state.recipe)
        addRecipeView.renderMessage();
        bookmarksView.render(model.state.bookmarks);
        window.history.pushState(null, '', `#${model.state.recipe.id}`)
        setTimeout(function(){
          addRecipeView.toggleWindow()
        }, MODAL_CLOSE_SEC)
      })
       .catch(err => addRecipeView.renderError(err.message))

}
const init = function() {
  recipeView.addHandlerRender(controlRecipes);
  recipeView.addHandlerUpdateServings(controlServings);
  recipeView.addHandlerAddBookmark(controlAddBookmark);
  searchView.addHandlerSearch(controlSearchResults);
  paginationView.addHandlerClick(controlPagination);
  addRecipeView._addHandlerUpload(controlAddRecipe)
}


init();