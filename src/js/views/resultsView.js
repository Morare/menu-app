import View from "./View";
import icons from "url:../../img/icons.svg";
class ResultsView extends View {
    _parentElement = document.querySelector(".results");
    _errorMessage = "No recipies found for you query! Please try again" 
    _message= ""

    _generateMarkup() {
        return this._data.map(res => this._generateMarkupPreview(res)).join("")
    }

    _generateMarkupPreview({image, title, publisher, id}) {
        const currentId = window.location.hash.slice(1)

        return `
                <li class="preview">
                    <a class="preview__link ${ currentId === id ? 'preview__link--active' :''}" href="#${id}">
                    <figure class="preview__fig">
                        <img src="${image}" alt="${title}"/>
                    </figure>
                    <div class="preview__data">
                        <h4 class="preview__title">${title}</h4>
                        <p class="preview__publisher">${publisher}</p>
                        <div class="preview__user-generated">
                        <svg>
                            <use href="${icons}#icon-user"></use>
                        </svg>
                        </div>
                    </div>
                    </a>
            </li>
        `
    }
}

export default new ResultsView();