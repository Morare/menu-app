import View from "./View";
import icons from "url:../../img/icons.svg";
class PaginationView extends View {
    _parentElement = document.querySelector(".pagination");
    addHandlerClick(handler) {
        this._parentElement.addEventListener('click', function(e) {
            const btn = e.target.closest('.btn--inline');
            if(btn) {
                const gotToPage = +btn.dataset.gotto;
                handler(gotToPage);
            }
        })
    }

    _generateMarkup() {
        const numPages = Math.ceil(this._data.results.length / this._data.resultsPerPage)
        const currentPage = this._data.page;
        /// Page 1, and there are other pages
        if(currentPage === 1 && numPages > 1) {
            return this._generateMarkupRightArrow(currentPage);
        }

        /// Page 1, and there no are other pages 
        if(currentPage === numPages && numPages > 1) {
            return this._generateMarkupLeftArrow(currentPage);
        }

        // Other pages
        if(currentPage < numPages ) {
            return this._generateMarkupLeftArrow(currentPage) + this._generateMarkupRightArrow(currentPage);
        }

        return ""
    }

    _generateMarkupLeftArrow(currentPage) {
        return this._generateMarkupForArrow(currentPage, -1, "prev", "left");
    }

    _generateMarkupRightArrow(currentPage) {
        return this._generateMarkupForArrow(currentPage, 1, "next", "right");
    }

    _generateMarkupForArrow(currentPage, number, direction, iconDirection){
        return `
        <button data-gotto="${currentPage + number}" class="btn--inline pagination__btn--${direction}">
            <svg class="search__icon">
            <use href="${icons}#icon-arrow-${iconDirection}"></use>
            </svg>
            <span>Page ${currentPage + number}</span>
        </button>
        `
    }

}

export default new PaginationView();